package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;
public class TC009EditContactTest extends Annotations {
	@BeforeTest
	public void setData() {
		testcaseName = "TC009EditContactTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName="TC009EditContactPositive";
	}
	@Test(dataProvider="fetchData", dependsOnMethods="com.autoBot.testcases.TC008CreateContactTest.CreateContact")
	public void EditLead(String fName,String lName) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickContactsTab()
		.ClickFindContactMenu()
		.EnterFirstName(fName)
		.ClickOnFindContactsButton()
		.ClickOnFirstResult()
		.ClickOnEditButton()
		.ClearAndEnterLastName(lName)
		.ClickOnUpdateButton()
		.VerifyLastNameForEditContact(lName);
		
	}

}
