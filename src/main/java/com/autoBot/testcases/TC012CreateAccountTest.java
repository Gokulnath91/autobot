package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;


public class TC012CreateAccountTest extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC012CreateAccountTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName="TC012CreateAccountPositive";
	}
	@Test(dataProvider="fetchData")
	public void CreateAccount(String AccName,String LocalName) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickAccountsTab()
		.ClickOnCreateAccountMenu()
		.EnterAccountName(AccName)
		.EnterLocalName(LocalName)
		.ClickOnCreateAccountButton()
		.VerifyCreateAccount(AccName);
		
		}
	

}
