package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC010MergeContactTest extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC010MergeContactTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName="TC010MergeContactPositive";
	}
	@Test(dataProvider="fetchData", dependsOnMethods="com.autoBot.testcases.TC009EditContactTest.EditLead")
	public void MergeContact(String fName1,String fName2,String ErrorMessage) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickContactsTab()
		.ClickMergeContactMenu()
		.ClickOnFromContactIcon()
		.NavigateToFindContact()
		.EnterFirstName(fName1)
		.StoreContactIDtoVerify()
		.ClickOnFirstResultToMergeContact()
		.NavigateToMergeContactPage()
		.ClickOnToContactIcon()
		.NavigateToFindContact()
		.EnterFirstName(fName2)
		.ClickOnFindContactsButton()
		.ClickOnFirstResultToMergeContact()
		.NavigateToMergeContactPage()
		.ClickOnMergContactButton()
		.ClickOnAccecptPopup()
		.ClickFindContactMenu()
		.EnterContactID()
		.ClickOnFindContactsButton()
		.VerifyMergeContact(ErrorMessage);
	}
	

}
