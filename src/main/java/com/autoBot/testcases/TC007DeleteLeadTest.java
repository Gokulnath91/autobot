package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;



public class TC007DeleteLeadTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC007DeleteLeadTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC007DeleteLeadPositive";
	}

	@Test(dataProvider = "fetchData", dependsOnMethods="com.autoBot.testcases.TC006DuplicateLeadTest.DuplicateLead")
	public void DeleteLead(String FisrtName , String ErrorMessage) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickLeadsTab()
		.ClickFindLeadsMenu()
		.EnterFirstName(FisrtName)
		.ClickOnFindLeadsButton()
		.StoreDelteLeadID()
		.ClickOnFirstResult()
		.ClickOnDeleteButton()
		.ClickFindLeadsMenu()
		.EnterTheDeleteLeadIDToVerify()
		.ClickOnFindLeadsButton()
		.VerifyDeleteLead(ErrorMessage);
	}

}
