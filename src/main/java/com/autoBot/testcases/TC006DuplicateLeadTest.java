package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;



public class TC006DuplicateLeadTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC006DuplicateLeadTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC006DuplicateLeadPositive";
	}

	@Test(dataProvider = "fetchData", dependsOnMethods="com.autoBot.testcases.TC005MergeLeadTest.MergeLead")
	public void DuplicateLead(String FisrtName) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickLeadsTab()
		.ClickFindLeadsMenu()
		.EnterFirstName(FisrtName)
		.ClickOnFindLeadsButton()
		.ClickOnFirstResult()
		.ClickOnDuplicateLeadButton()
		.ClikOnCreateLeadButton()
		.VerifyFirstNameForDuplicateLead(FisrtName);
	}

}
