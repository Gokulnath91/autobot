package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;


public class TC014MergeAccountTest extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC014MergeAccountTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		
		excelFileName="TC014MergeAccountPositive";
	}
	@Test(dataProvider="fetchData", dependsOnMethods="com.autoBot.testcases.TC013EditAccountTest.EditAccount")
	public void MergeContact(String AccName1,String AccName2,String ErrorMessage) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickAccountsTab()
		.ClickOnMergeAccountsMenu()
		.ClickOnFromAccountIcon()
		.NavigateToFindAccountsPage()
		.EnterAccountName(AccName1)
		.ClickOnFindAccountsButton()
		.StoreAccountIDToVerify()
		.ClickOnFirstResultToMergeAccount()
		.NavigateTOMergeAccountPage()
		.ClickOnToAccountIcon()
		.NavigateToFindAccountsPage()
		.EnterAccountName(AccName2)
		.ClickOnFindAccountsButton()
		.ClickOnFirstResultToMergeAccount()
		.NavigateTOMergeAccountPage()
		.ClickOnMergeAccountButton()
		.ClickOnAcceptPopup()
		.ClickOnFindAccountsMenu()
		.EnterAccountID()
		.ClickOnFindAccountsButton()
		.VerifyMergeAccount(ErrorMessage);
	
	
	}
	

}
