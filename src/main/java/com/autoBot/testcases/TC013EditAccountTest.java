package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC013EditAccountTest extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC013EditAccountTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName="TC013EditAccountPositive";
	}
	@Test(dataProvider="fetchData", dependsOnMethods="com.autoBot.testcases.TC012CreateAccountTest.CreateAccount")
	public void EditAccount(String AccName,String LocalName) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickAccountsTab()
		.ClickOnFindAccountsMenu()
		.EnterAccountName(AccName)
		.ClickOnFindAccountsButton()
		.StoreAccountIDToVerify()
		.ClickOnFirstResult()
		.ClickOnEditButton()
		.ClearAndUpdateLocalName(LocalName)
		.ClickOnSaveButton()
		.VerifyLocalNameForEditAccount(LocalName);
		}
	

}
