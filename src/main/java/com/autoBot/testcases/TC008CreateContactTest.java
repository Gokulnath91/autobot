package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC008CreateContactTest extends Annotations {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC008CreateContactTest";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName="TC008CreateContactPositive";
	}
	@Test(dataProvider="fetchData")
	public void CreateContact(String fName, String lName) {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickContactsTab()
		.ClickCreateContactMenu()
		.EnterFirstName(fName)
		.EnterLastName(lName)
		.ClickOnCreateContactButton()
		.VerifyFirstNameForCreateContact(fName);
	}
	
}
