package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;

import com.autoBot.testng.api.base.Annotations;

public class TC003LoginWithInvalidCredentialTest extends Annotations {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003LoginWithInvalidCredentialTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC003LoginWithInvalidCredential";
	}
	
	@Test(dataProvider = "fetchData")
	public void loginLogout(String userName, String password, String data) throws InterruptedException {
		new LoginPage()
		.enterUserName(userName)
		.enterPassWord(password)
		.clickLoginButtonWithInvalidCredential()
		.VerifyLoginStatus(data);
		
	}

	
	
	
	
	
	
	
	
	
	

}
