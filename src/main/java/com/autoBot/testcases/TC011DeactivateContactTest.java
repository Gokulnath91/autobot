package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC011DeactivateContactTest extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC011DeactivateContactTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName="TC011DeactivateContactPositive";
	}
	@Test(dataProvider="fetchData", dependsOnMethods="com.autoBot.testcases.TC010MergeContactTest.MergeContact")
	public void DeactivateContact(String fName,String ErrorMessage) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickContactsTab()
		.ClickFindContactMenu()
		.EnterFirstName(fName)
		.ClickOnFindContactsButton()
		.StoreContactIDtoVerify()
		.ClickOnFirstResult()
		.ClickOnDeactiveButton()
		.ClickOnAccecptPopup()
		.ClickFindContactMenu()
		.EnterContactID()
		.ClickOnFindContactsButton()
		.VerifyDeactivateContact(ErrorMessage);
		
		
		
		}
	

}
