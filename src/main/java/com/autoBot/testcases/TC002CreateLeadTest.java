package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;


public class TC002CreateLeadTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC002CreateLeadTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC002CreateLeadPositive";
	}
	

	@Test(dataProvider = "fetchData")
	public void CreateLead(String CName, String FisrtName, String LastName) {
		new LoginPage()
		.enterUserName("DemoSalesManager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickLeadsTab()
		.ClickCreateLeadMenu()
		.EnterCompanyName(CName)
		.EnterFirstName(FisrtName)
		.EnterLastName(LastName)
		.ClickCreateLeadButton()
		.VerifyFirstNameForCreateLead(FisrtName);
			
	
	
	
	}

}
