package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;


public class TC005MergeLeadTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC005MergeLeadTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC005MergeLeadPositive";
	}

	@Test(dataProvider = "fetchData", dependsOnMethods="com.autoBot.testcases.TC004EditLeadTest.EditLead")
	public void MergeLead(String FisrtName1, String FisrtName2,String errorMessage) throws InterruptedException {
		new LoginPage()
		.enterUserName("demosalesmanager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickLeadsTab()
		.ClickMergeLeadMenu()
		.ClickFromLeadIcon()
		.NavidateToFindLeads()
		.EnterFirstName(FisrtName1)
		.ClickOnFindLeadsButton()
		.StoreToLeadIDToVerify()
		.ClickOnFirstResultToMergeLead()
		.NavigateToMergeLeadwindow()
		.ClickToLeadIcon()
		.NavidateToFindLeads()
		.EnterFirstName(FisrtName2)
		.ClickOnFindLeadsButton()
		.ClickOnFirstResultToMergeLead()
		.NavigateToMergeLeadwindow()
		.ClickMergeLeadButton()
		.AcceptPopup()
		.ClickFindLeadsMenu()
		.EnterTheLeadIDToVerify()
		.ClickOnFindLeadsButton()
		.VerifyMergeLead(errorMessage);
	}

}
