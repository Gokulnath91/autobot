package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;


public class TC004EditLeadTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC004EditLeadTest";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC004EditLeadPositive";
	}

	@Test(dataProvider = "fetchData", dependsOnMethods="com.autoBot.testcases.TC002CreateLeadTest.CreateLead")
	public void EditLead(String CName, String FisrtName) throws InterruptedException {
		new LoginPage()
		.enterUserName("DemoSalesManager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.ClickCrmsfaButton()
		.ClickLeadsTab()
		.ClickFindLeadsMenu()
		.EnterFirstName(FisrtName)
		.ClickOnFindLeadsButton()
		.ClickOnFirstResult()
		.ClickOnEditButton()
		.ClearAndEnterCompanyName(CName)
		.ClickOnUpdateButton()
		.VerifyCompanyNameForEditLead(CName);
	}

}
