package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage VerifyFirstNameForCreateLead(String data) {
		WebElement eleVerifyName = locateElement("id","viewLead_firstName_sp");
		verifyExactText(eleVerifyName, data);
		return this;
	}
	
	public EditLeadPage ClickOnEditButton() {
		WebElement eleEditButton = locateElement("xpath", "//a[text()='Edit']");
		click(eleEditButton);
		return new EditLeadPage();
	}
	
	public void VerifyCompanyNameForEditLead(String data) {
		
		WebElement eleCompanyName = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(eleCompanyName,data);
	}
			
	public DuplicateLead ClickOnDuplicateLeadButton() {
		WebElement eleDeleteLeadButton = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(eleDeleteLeadButton);		
		return new DuplicateLead();
	}
	
	public ViewLeadPage VerifyFirstNameForDuplicateLead(String data) {
		WebElement eleFirstName = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFirstName, data);
		return this;
	}
	
	public MyLeadsPage ClickOnDeleteButton() {
		WebElement eleDeleteButton = locateElement("xpath","//a[text()='Delete']");
		click(eleDeleteButton);
		return new MyLeadsPage();
	}
	
}
