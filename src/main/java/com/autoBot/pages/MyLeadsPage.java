package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage ClickCreateLeadMenu() {
		WebElement eleCeateLead = locateElement("link","Create Lead");	
		click(eleCeateLead);
		return new CreateLeadPage();

	}
	
	public FindLeadsPage ClickFindLeadsMenu() {
		WebElement eleFindLead = locateElement("link", "Find Leads");
		click(eleFindLead);
		return new FindLeadsPage();
	}
	
	public MergeLeadPage ClickMergeLeadMenu() {
	  WebElement eleMergeLead = locateElement("link", "Merge Leads");
		click(eleMergeLead);
		return new MergeLeadPage();
	}
	
	
	
	
	
	
	

}
