package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ContactsPage extends Annotations {
	
	public CreateContact ClickCreateContactMenu() {
		WebElement eleCreateMenu = locateElement("link","Create Contact");
		click(eleCreateMenu);
		return new CreateContact();
	}
	
	public FindContactPage ClickFindContactMenu() {
		WebElement eleFindMenu = locateElement("link","Find Contacts");
		click(eleFindMenu);
		return new FindContactPage();
	}
	
	public MergeContactPage ClickMergeContactMenu() {
		WebElement eleMergeMenu = locateElement("link","Merge Contacts");
		click(eleMergeMenu);
		driver.findElementByLinkText("Merge Contacts").click();
		return new MergeContactPage();
	}

}
