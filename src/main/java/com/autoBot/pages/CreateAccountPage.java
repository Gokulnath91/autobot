package com.autoBot.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateAccountPage extends Annotations {
	
	public CreateAccountPage EnterAccountName(String data) {
		WebElement eleName = locateElement("id", "accountName");
		clearAndType(eleName, data);
		return this;
	}

	public CreateAccountPage EnterLocalName(String data) {
		WebElement eleLocalName = locateElement("id", "groupNameLocal");
		clearAndType(eleLocalName, data);
		return this;
	}
	
	public AccountDetailsPage ClickOnCreateAccountButton() {
		WebElement eleCreate = locateElement("class", "smallSubmit");
		click(eleCreate);
		try {
			WebElement eleErrorMessage = locateElement("xpath", "//span[@class='errorMessage']");
			if (verifyPartialText(eleErrorMessage, "Duplicate")) {
				WebElement eleCreateDuplicate = locateElement("xpath", "//a[@class='buttonDangerous']");
				click(eleCreateDuplicate);
				switchToAlert();
				acceptAlert();
				
			}	
		} catch (NoSuchElementException e) {
			
		}
		return new AccountDetailsPage();
	}
	
}
