package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class FindContactPage extends Annotations {
	
	public FindContactPage EnterFirstName(String data) {
		WebElement eleFirstName = locateElement("xpath", "(//div[@class='x-tab-panel-bwrap'])//input[@name='firstName']");
		clearAndType(eleFirstName, data);
		return this;
				
	}
	
	public FindContactPage ClickOnFindContactsButton() {
		WebElement eleFindButton = locateElement("xpath", "//button[text()='Find Contacts']");
		click(eleFindButton);
		return this;
				
	}
	
	public ViewContactPage ClickOnFirstResult() throws InterruptedException {
		//Thread.sleep(1500);
		WebElement eleFirstResult = locateElement("xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		click(eleFirstResult);
		return new ViewContactPage();
			
	}
	
	public FindContactPage StoreContactIDtoVerify() throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleDataToVerify = locateElement("xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		dataToVerify = getElementText(eleDataToVerify);
		return this;
				
	}
		
	public FindContactPage ClickOnFirstResultToMergeContact() throws InterruptedException {
		//Thread.sleep(1500);
		WebElement eleFirstResult = locateElement("xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		click(eleFirstResult);
		return this;
			
	}
	
	public MergeContactPage NavigateToMergeContactPage() {
		switchToWindow(0);
		return new MergeContactPage();
	}
	
	public FindContactPage EnterContactID() {
		WebElement eleEnterID = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleEnterID, dataToVerify);
		return this;
				
	}
	
	public FindContactPage VerifyMergeContact(String data) throws InterruptedException {
		Thread.sleep(1500);
		WebElement Message = locateElement("xpath", "//div[@class='x-panel-bbar']//div[@class='x-paging-info']");
		verifyExactText(Message, data);
		
		return this;
				
	}
	
	public FindContactPage VerifyDeactivateContact(String data) throws InterruptedException {
		Thread.sleep(1500);
		WebElement Message = locateElement("xpath", "//div[@class='x-panel-bbar']//div[@class='x-paging-info']");
		verifyExactText(Message, data);

		
		return this;
				
	}
	
}
