package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ViewContactPage extends Annotations {
	
	public ViewContactPage VerifyFirstNameForCreateContact(String data) {
		WebElement eleFirstName = locateElement("id","viewContact_firstName_sp");
		verifyExactText(eleFirstName, data);
		return this;
	}
	
	public EditContact ClickOnEditButton() {
		WebElement eleEditButon = locateElement("link", "Edit");
		click(eleEditButon);
		return new EditContact();
	}
	
	public ViewContactPage VerifyLastNameForEditContact(String data) {
		WebElement eleLastName = locateElement("id","viewContact_lastName_sp");
		verifyExactText(eleLastName, data);
		return this;
		
	}
	
	public ViewContactPage ClickOnDeactiveButton() {
		WebElement eleDeacivateButon = locateElement("link", "Deactivate Contact");
		click(eleDeacivateButon);
		return this;
	}

	public ContactsPage ClickOnAccecptPopup() {
		switchToAlert();
		acceptAlert();
		return new ContactsPage();
		
	}

	
}
