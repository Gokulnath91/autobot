package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class EditLeadPage extends Annotations {
	
	public EditLeadPage ClearAndEnterCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		clearAndType(eleCompanyName, data);
		return this;
	}

	public ViewLeadPage ClickOnUpdateButton() {
		WebElement eleUpdateButton = locateElement("xpath", "//input[@value='Update']");
		click(eleUpdateButton);
		return new ViewLeadPage();
	}
	
	
	
	
	
}
