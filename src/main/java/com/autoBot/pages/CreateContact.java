package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateContact extends Annotations {
	
	public CreateContact EnterFirstName(String data) {
		WebElement eleFirstName = locateElement("id", "firstNameField");
		clearAndType(eleFirstName, data);
		return this;
		
	}
	
	public CreateContact EnterLastName(String data) {
		WebElement eleLastName = locateElement("id", "lastNameField");
		clearAndType(eleLastName, data);
		return this;
		
	}
	
	public ViewContactPage ClickOnCreateContactButton() {
		WebElement eleCreateButton = locateElement("class", "smallSubmit");
		click(eleCreateButton);
		return new ViewContactPage();
		
	}
	
}
