package com.autoBot.pages;


import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class MyHomePage extends Annotations {

	public MyLeadsPage ClickLeadsTab() {
		WebElement eleLead = locateElement("link" ,"Leads");
		click(eleLead);
		return new MyLeadsPage();
	}
	
	public ContactsPage ClickContactsTab() {
		WebElement eleContact = locateElement("link" ,"Contacts");
		click(eleContact);
		return new ContactsPage();
	}
	
	public AccountsPage ClickAccountsTab() {
		WebElement eleAccount = locateElement("link" ,"Accounts");
		click(eleAccount);
		return new AccountsPage();
	}	
	
	
}
