package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class AccountDetailsPage extends Annotations {
	
	public AccountDetailsPage VerifyCreateAccount(String data) {
		WebElement eleName = locateElement("xpath","//span[text()='Account Name']/following::span[1]");
		verifyPartialText(eleName, data);
		return this;
	}
	
	public EditAccountPage ClickOnEditButton() {
		WebElement eleButton = locateElement("link","Edit");
		click(eleButton);
		return new EditAccountPage();
		
	}
	
	public AccountDetailsPage VerifyLocalNameForEditAccount(String data) {
		
		WebElement eleLocalName = locateElement("xpath", "//span[text()='Account Name']/following::span[4]");
		verifyExactText(eleLocalName, data);
		
		return this;
	}
	
	
	
	
	

}
