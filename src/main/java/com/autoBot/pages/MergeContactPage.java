package com.autoBot.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MergeContactPage extends Annotations {
	
	public MergeContactPage ClickOnFromContactIcon() {
		WebElement eleFromIcon = locateElement("xpath", "(//td[@class='titleCell']/following::a)[1]");
		click(eleFromIcon);
		return this;
		
	}
	
	public FindContactPage NavigateToFindContact() {
		switchToWindow(2);
		return new FindContactPage();
	}
	
	public MergeContactPage ClickOnToContactIcon() {
		WebElement eleToIcon = locateElement("xpath", "(//td[@class='titleCell']/following::a)[2]");
		click(eleToIcon);
		return this;
		
	}
	
	
	
	public MergeContactPage ClickOnMergContactButton() {
		WebElement eleMergeButton = locateElement("link", "Merge");
		click(eleMergeButton);
		return this;
		
	}
	
	public ContactsPage ClickOnAccecptPopup() {
		switchToAlert();
		acceptAlert();
		return new ContactsPage();
		
	}
	
	

}
