package com.autoBot.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MergeAccontsPage extends Annotations {

	public MergeAccontsPage ClickOnFromAccountIcon() {
		WebElement eleIcon = locateElement("xpath", "(//td[@class='titleCell']/following::a)[1]");
		click(eleIcon);
		return this;
		
	}
	
	public FindAccountsPage NavigateToFindAccountsPage() {
		
		switchToWindow(2);
		return new FindAccountsPage();
	}
	
	public MergeAccontsPage ClickOnToAccountIcon() {
		WebElement eleToIcon = locateElement("xpath", "(//td[@class='titleCell']/following::a)[2]");
		click(eleToIcon);
		return this;
		
	}
	
	public MergeAccontsPage ClickOnMergeAccountButton() {
		WebElement eleMergeButton = locateElement("link", "Merge");
		click(eleMergeButton);
		return this;
	}
	
	public AccountsPage ClickOnAcceptPopup() {
		switchToAlert();
		acceptAlert();
		return new AccountsPage();
		
	}
	
	
	
	
	
	
	
}
