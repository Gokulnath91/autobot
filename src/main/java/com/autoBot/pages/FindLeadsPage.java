package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations {

	public FindLeadsPage EnterFirstName(String data) {
		WebElement eleFirstName = locateElement("xpath", "(//div[@class='gwtWidget subSectionBlock']//input)[2]");
		clearAndType(eleFirstName, data);
		return this;
	}

	public FindLeadsPage ClickOnFindLeadsButton() {
		WebElement eleFindButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindButton);
		return this;
	}

	public ViewLeadPage ClickOnFirstResult() throws InterruptedException {
		// Thread.sleep(1500);
		WebElement eleFirstResult = locateElement("xpath",
				"(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[1])[1]");
		click(eleFirstResult);
		return new ViewLeadPage();
	}

	public FindLeadsPage StoreToLeadIDToVerify() throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleToLeadID = locateElement("xpath",
				"(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[1])[1]");
		dataToVerify = getElementText(eleToLeadID);
		return this;
	}

	public MergeLeadPage NavigateToMergeLeadwindow() {
		switchToWindow(0);
		return new MergeLeadPage();
	}

	public FindLeadsPage EnterTheLeadIDToVerify() {
		WebElement eleID = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleID, dataToVerify);
		return this;
	}

	public FindLeadsPage EnterTheDeleteLeadIDToVerify() {
		WebElement eleID = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleID, DeleteLeadID);
		return this;
	}

	public FindLeadsPage VerifyMergeLead(String data) throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleMergeLead = locateElement("xpath", "//div[@class='x-paging-info']");
		verifyPartialText(eleMergeLead, data);
		return this;
	}

	public FindLeadsPage ClickOnFirstResultToMergeLead() throws InterruptedException {
		// Thread.sleep(1500);
		WebElement eleResult = locateElement("xpath",
				"(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[1])[1]");
		click(eleResult);
		return this;
	}

	public FindLeadsPage StoreDelteLeadID() throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleLeadID = locateElement("xpath",
				"(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[1])[1]");
		DeleteLeadID = getElementText(eleLeadID);
		return this;
	}

	public FindLeadsPage VerifyDeleteLead(String data) throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleMessage = locateElement("xpath", "//div[@class='x-paging-info']");
		verifyExactText(eleMessage, data);
		return this;
	}

}
