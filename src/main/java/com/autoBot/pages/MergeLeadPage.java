package com.autoBot.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MergeLeadPage extends Annotations {
	
	public MergeLeadPage ClickFromLeadIcon() {
		WebElement eleIcon = locateElement("xpath","//input[@id='partyIdFrom']/following-sibling::a[1]");
		click(eleIcon);
		return this;
	}
	
	public FindLeadsPage NavidateToFindLeads() {
		switchToWindow(2);
		return new FindLeadsPage();
	}
	
	public MergeLeadPage ClickToLeadIcon() {
		WebElement eleIcon = locateElement("xpath", "//input[@id='partyIdTo']/following-sibling::a[1]");
		click(eleIcon);
		return this;
	}
	
	public MergeLeadPage ClickMergeLeadButton() {
		WebElement eleButton = locateElement("link", "Merge");
		click(eleButton);
		return this;
	}
	
	public MyLeadsPage AcceptPopup() {
		switchToAlert();
		acceptAlert();
		return new MyLeadsPage();
	}
	
	

}
