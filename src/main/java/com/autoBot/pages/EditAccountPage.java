package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class EditAccountPage extends Annotations {
	
	
	public EditAccountPage ClearAndUpdateLocalName(String data) {
		WebElement eleLocalName = locateElement("id", "groupNameLocal");
		clearAndType(eleLocalName, data);
		return this;
		
	}
	
	public AccountDetailsPage ClickOnSaveButton() {
		WebElement eleSaveButton = locateElement("class", "smallSubmit");
		click(eleSaveButton);
		return new AccountDetailsPage();

		
	}

}
