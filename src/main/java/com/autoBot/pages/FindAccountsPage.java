package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class FindAccountsPage extends Annotations {

	public FindAccountsPage EnterAccountName(String data) {
		WebElement eleName = locateElement("xpath", "(//div[@class='x-tab-panel-bwrap'])//input[@name='accountName']");
		clearAndType(eleName, data);
		return this;
	}
	
	public FindAccountsPage ClickOnFindAccountsButton() {
		WebElement eleButton = locateElement("xpath", "//button[text()='Find Accounts']");
		click(eleButton);
		return this;
	}
	
	public FindAccountsPage StoreAccountIDToVerify() throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleId = locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
		dataToVerify = getElementText(eleId);
		return this;
	}
	
	public AccountDetailsPage ClickOnFirstResult() {
		WebElement eleResult = locateElement("xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		click(eleResult);
		return new AccountDetailsPage();
	}
	
	public FindAccountsPage ClickOnFirstResultToMergeAccount() throws InterruptedException {
		//Thread.sleep(1500);
		WebElement eleFirstResult = locateElement("xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		click(eleFirstResult);
		return this;
	}
	
	public MergeAccontsPage NavigateTOMergeAccountPage() {
		switchToWindow(0);
		return new MergeAccontsPage();

	}
	
	
	public FindAccountsPage EnterAccountID() {
		WebElement eleAccID = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleAccID, dataToVerify);
		return this;
	}
	public FindAccountsPage VerifyMergeAccount(String data) throws InterruptedException {
		Thread.sleep(1500);
		WebElement eleMessage = locateElement("xpath", "//div[@class='x-paging-info']");
		verifyPartialText(eleMessage, data);
		return this;
	
	}
	
	
}
