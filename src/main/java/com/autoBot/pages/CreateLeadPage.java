package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage EnterCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompanyName, data);		
		return this;

	}
	
	public CreateLeadPage EnterFirstName(String data) {
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName, data);
		return this;

	}
	
	public CreateLeadPage EnterLastName(String data) {
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName, data);	
		return this;

	}
	
	public ViewLeadPage ClickCreateLeadButton() {
		WebElement eleCreateLeadButton = locateElement("xpath","//input[@name='submitButton']");
		click(eleCreateLeadButton);
		return new ViewLeadPage();

	}

}
