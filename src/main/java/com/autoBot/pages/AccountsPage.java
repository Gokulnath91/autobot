package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class AccountsPage extends Annotations {
	
	public CreateAccountPage ClickOnCreateAccountMenu() {
		WebElement eleCreateMenu = locateElement("link", "Create Account");
		click(eleCreateMenu);
		return new CreateAccountPage();
		
	}
	
	public FindAccountsPage ClickOnFindAccountsMenu() {
		WebElement eleFindAccountMenu = locateElement("link", "Find Accounts");
		click(eleFindAccountMenu);
		driver.findElementByLinkText("Find Accounts").click();
		return new FindAccountsPage();
		
	}
	
	public MergeAccontsPage ClickOnMergeAccountsMenu() {
		WebElement eleMergeAccountMenu = locateElement("link", "Merge Accounts");
		click(eleMergeAccountMenu);
		return new MergeAccontsPage();
		
	}

}
