package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class EditContact extends Annotations {
	
	

	public EditContact ClearAndEnterLastName(String data) {
		WebElement eleLastName = locateElement("id", "updateContactForm_lastName");
		clearAndType(eleLastName, data);
		return this;
		
	}
	
	public ViewContactPage ClickOnUpdateButton() {
		WebElement eleUpdateButton = locateElement("class", "smallSubmit");
		click(eleUpdateButton);
		return new ViewContactPage();
	}
	
	
}
