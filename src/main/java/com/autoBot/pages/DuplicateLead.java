package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class DuplicateLead extends Annotations{
	
	public ViewLeadPage ClikOnCreateLeadButton() {
		WebElement eleCreateButton = locateElement("xpath", "//input[@name='submitButton']");
		click(eleCreateButton);
		return new ViewLeadPage();
	}

}
